<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<?php $categories = get_categories(); ?>
<?php $category_id = $categories[1]->term_id; ?>
<div class="page-container">
    <?php if (!empty(get_field('slider_top'))) : ?>
    <div class="slide">
        <?php $slider_top = get_field('slider_top'); ?>
        <?php foreach ($slider_top as $top) : ?>
        <div class="slide-casino">
            <div class="slider-container-casino">
                <div class="slide-casino-content">
                    <div class="slide-casino-logo">
                        <a href="<?php echo get_field('affialite_link', $top->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $top->ID); ?>"></a>
                    </div>
                    <div class="slide-casino-bonus">
                        <h3><?php echo get_field('bonus', $top->ID); ?></h3>
                        <h4><?php echo get_field('bonus_bottom_text', $top->ID); ?></h4>
                    </div>
                    <div class="slide-casino-name">
                        <p><?php echo $top->post_title; ?></p>
                    </div>
                </div>
                <div class="slide-casino-button">
                    <a href="<?php echo get_field('affialite_link', $top->ID); ?>" target="_blank">Visit<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" class="svg-inline--fa fa-angle-double-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg></a>
                </div>
            </div>
            <?php if (!empty(get_field('terms_info', $top->ID))) : ?>
            <div class="slide-terms">
                <p><?php echo get_field('terms_info', $top->ID); ?>
                    <a href="<?php echo get_field('terms_link', $top->ID); ?>" target="_blank">18+. Terms & Conditions apply</a><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                </p>
            </div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="home-wave">
        <iframe class="animation" src="animation.html"></iframe>
        <div class="home-wave-container-invisible" style="">
            <div class="home-wave-logo">
                <?php $intro_block = get_field('intro_block'); ?>
                <img src="<?php echo $intro_block['logo']; ?>">
            </div>
            <div class="home-wave-text">
                <a href="<?php echo $intro_block['text']['url']; ?>">
                    <h1><?php echo $intro_block['text']['title']; ?></h1>
                </a>
            </div>
        </div>
        <div class="home-wave-container">
            <div class="home-wave-logo">
                <?php $intro_block = get_field('intro_block'); ?>
                <img src="<?php echo $intro_block['logo']; ?>">
            </div>
            <div class="home-wave-text">
                <a href="<?php echo $intro_block['text']['url']; ?>">
                    <h1><?php echo $intro_block['text']['title']; ?></h1>
                </a>
            </div>
        </div>
    </div>
    <div class="page-content">
        <div class="recommended-casinos">
            <div class="recommended-casinos-text">
                <?php $featured_top_companies_block = get_field('featured_top_companies_block'); ?>
                <h2><?php echo $featured_top_companies_block['title']; ?><svg version="1.1" id="Capa_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 244.4 216.7" style="enable-background:new 0 0 244.4 216.7;" xml:space="preserve"><metadata><sfw  xmlns="&ns_sfw;"><slices></slices><sliceSourceBounds  bottomLeftOrigin="true" height="217.1" width="244.3" x="0" y="-0.1"></sliceSourceBounds></sfw></metadata><path class="st0" d="M226.6,113.8h-5.1c9.9,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16-17.7,16h-8.3c9.9,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16-17.7,16h-4c9.8,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16.1-17.7,16.1H95.8c-10.7,0-19.3-8.7-19.3-19.3v-79.2l43.3-69.6c1.5-2.4,2.1-5.1,1.8-7.9l-2.7-27C118.3,7,122.8,0.9,129.4,0c10.6-1.5,25.8,2.1,33.7,30.6c0,0,2.3,33.6-3.7,48.8h67.8c9.9,0,17.8,8.3,17.1,18.3C243.7,106.9,235.7,113.8,226.6,113.8z"/><path class="st1" d="M62.6,101.6H13.8c-7.6,0-13.8,6.2-13.8,13.8v87.5c0,7.6,6.2,13.8,13.8,13.8h48.8c7.6,0,13.8-6.2,13.8-13.8v-87.5C76.5,107.8,70.3,101.6,62.6,101.6z"/><circle class="st2" cx="38.2" cy="202.7" r="8.5"/></svg><span><?php echo $featured_top_companies_block['bottom_title']; ?></span></h2>
            </div>
            <?php if (!empty($featured_top_companies_block['featured_top_companies'])) : ?>
                <div class="recommended-casinos-container">
                    <?php foreach ($featured_top_companies_block['featured_top_companies'] as $key => $featured_company) : ?>
                        <div class="recommended-casinos-box <?php echo $key === 0 ? 'background-featured' : ''; ?>">
                            <div class="recommended-casinos-box-name">
                                <p><?php echo $featured_company->post_title; ?></p>
                            </div>
                            <div class="recommended-casinos-box-rating">
                                <div class="star-ratings">
                                    <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_company->ID); ?>%">
                                        <span>●●●●●</span>
                                    </div>
                                    <div class="empty-ratings">
                                        <span>●●●●●</span>
                                    </div>
                                </div>
                            </div>
                            <div class="recommended-casinos-box-logo">
                                <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank"> <img src="<?php echo get_field('big_logo', $featured_company->ID); ?>"></a>
                            </div>
                            <div class="recommended-casinos-box-bonus">
                                <h3><?php echo get_field('bonus', $featured_company->ID); ?></h3>
                                <h4><?php echo get_field('bonus_bottom_text', $featured_company->ID); ?></h4>
                            </div>
                            <div class="recommended-casinos-box-terms-link">
                                <a href="<?php echo get_field('terms_link', $featured_company->ID); ?>" target="_blank">18+. Terms & Conditions apply</a>
                                <div class="recommended-casinos-box-svg">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                                </div>
                            </div>
                            <div class="mawkurs">
                                <div class="recommended-casinos-box-button-info">
                                    <div class="recommended-casinos-box-button">
                                        <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank">Visit Casino</a>
                                    </div>
                                    <?php if (!empty(get_field('terms_info', $featured_company->ID))) : ?>
                                        <div class="recommended-casinos-box-terms-info">
                                            <p><?php echo get_field('terms_info', $featured_company->ID); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <div class="recommended-casinos-paragraph">
                <?php $featured_bottom_block = get_field('featured_bottom_casino_block'); ?>
                <h2><?php echo $featured_bottom_block['title']; ?></h2>
            </div>
            <?php if (!empty($featured_bottom_block['featured_bottom_companies'])) : ?>
                <div class="recommended-casinos-container">
                    <?php foreach ($featured_bottom_block['featured_bottom_companies'] as $key => $featured_company_bot) : ?>
                        <div class="recommended-casinos-box <?php echo $key === 0 ? 'background-featured' : ''; ?>">
                            <div class="recommended-casinos-box-name">
                                <p><?php echo $featured_company_bot->post_title; ?></p>
                            </div>
                            <div class="recommended-casinos-box-rating">
                                <div class="star-ratings">
                                    <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_company_bot->ID); ?>%">
                                        <span>●●●●●</span>
                                    </div>
                                    <div class="empty-ratings">
                                        <span>●●●●●</span>
                                    </div>
                                </div>
                            </div>
                            <div class="recommended-casinos-box-logo">
                                <a href="<?php echo get_field('affialite_link', $featured_company_bot->ID); ?>" target="_blank"> <img src="<?php echo get_field('big_logo', $featured_company_bot->ID); ?>"></a>
                            </div>
                            <div class="recommended-casinos-box-bonus">
                                <h3><?php echo get_field('bonus', $featured_company_bot->ID); ?></h3>
                                <h4><?php echo get_field('bonus_bottom_text', $featured_company_bot->ID); ?></h4>
                            </div>
                            <div class="recommended-casinos-box-terms-link">
                                <a href="<?php echo get_field('terms_link', $featured_company_bot->ID); ?>" target="_blank">18+. Terms & Conditions apply</a>
                                <div class="recommended-casinos-box-svg">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                                </div>
                            </div>
                            <div class="mawkurs">
                                <div class="recommended-casinos-box-button-info">
                                    <div class="recommended-casinos-box-button">
                                        <a href="<?php echo get_field('affialite_link', $featured_company_bot->ID); ?>" target="_blank">Visit Casino</a>
                                    </div>
                                    <?php if (!empty(get_field('terms_info', $featured_company_bot->ID))) : ?>
                                        <div class="recommended-casinos-box-terms-info">
                                            <p><?php echo get_field('terms_info', $featured_company_bot->ID); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="online-casinos">
            <div class="online-casinos-text">
                <?php $featured_small_companies_block = get_field('featured_small_companies_block'); ?>
                <h2><?php echo $featured_small_companies_block['title']; ?><svg height="512" viewBox="0 0 192 192" width="512" xmlns="http://www.w3.org/2000/svg"><path fill="#ffc83d" d="m155.109 74.028a4 4 0 0 0 -3.48-2.028h-52.4l8.785-67.123a4.023 4.023 0 0 0 -7.373-2.614l-63.724 111.642a4 4 0 0 0 3.407 6.095h51.617l-6.962 67.224a4.024 4.024 0 0 0 7.411 2.461l62.671-111.63a4 4 0 0 0 .048-4.027z"/></svg><span><?php echo $featured_small_companies_block['bottom_title']; ?></span></h2>
            </div>
            <?php if (!empty($featured_small_companies_block['featured_companies_small'])) : ?>
            <div class="online-casinos-container">
                <?php foreach ($featured_small_companies_block['featured_companies_small'] as $key => $featured_small) : ?>
                <div class="online-casinos-box">
                    <div class="online-casinos-inside">
                        <div class="online-casinos-box-logo">
                            <a href="<?php echo get_field('affialite_link', $featured_small->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $featured_small->ID); ?>"></a>
                        </div>
                        <div class="online-casinos-box-name">
                            <p><?php echo $featured_small->post_title; ?></p>
                        </div>
                        <div class="online-casinos-box-rating">
                            <div class="star-ratings">
                                <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_small->ID); ?>%">
                                    <span>●●●●●</span>
                                </div>
                                <div class="empty-ratings">
                                    <span>●●●●●</span>
                                </div>
                            </div>
                        </div>
                        <div class="online-casinos-box-bonus">
                            <h3><?php echo get_field('bonus', $featured_small->ID); ?></h3>
                            <h4><?php echo get_field('bonus_bottom_text', $featured_small->ID); ?></h4>
                        </div>
                    </div>
                    <?php if (!empty(get_field('terms_info', $featured_small->ID))) : ?>
                    <div class="hide-paragraph">
                        <h4>Bonus Terms & Conditions</h4>
                        <p><?php echo get_field('terms_info', $featured_small->ID); ?></p>
                    </div>
                    <?php endif; ?>
                    <div class="online-casinos-box-terms-link-button">
                        <?php if (!empty(get_field('terms_info', $featured_small->ID))) : ?>
                            <div class="online-casinos-box-terms-link">
                                <a href="<?php echo get_field('terms_link', $featured_small->ID); ?>" target="_blank" class="open-paragraph">
                                    <p class="first-text">18+. Terms & Conditions apply</p>
                                    <p class="second-text">Read Full Terms</p>
                                </a>
                                <div class="online-casinos-box-svg">
                                    <p class="svg-text">HERE</p>
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                                </div>
                            </div>
                        <?php else : ?>
                            <a href="<?php echo get_field('terms_link', $featured_small->ID); ?>" target="_blank" class="open-paragraph">
                                <p class="first-text">18+. Terms & Conditions apply<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></p>
                            </a>
                        <?php endif; ?>
                        <div class="online-casinos-box-button">
                            <a href="<?php echo get_field('affialite_link', $featured_small->ID); ?>" target="_blank">Visit Casino</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="casino-guides">
            <div class="casino-guides-text">
                <?php $guides_block = get_field('guides_block'); ?>
                <h2><?php echo $guides_block['guides_title']; ?><svg height="512" viewBox="0 0 54 58" width="512" xmlns="http://www.w3.org/2000/svg"><g id="Page-1" fill="none" fill-rule="evenodd"><g id="012---E-Reader" fill-rule="nonzero"><path id="Shape" d="m38 58h-34c-2.209139 0-4-1.790861-4-4v-50c0-2.209139 1.790861-4 4-4h34c2.209139 0 4 1.790861 4 4v23.54z" fill="#35495e"/><path id="Shape" d="m38 4v24.51l-3.29 21.49h-30.71v-46z" fill="#ecf0f1"/><path id="Shape" d="m38 19.8v8.71l-3.29 21.49h-30.71v-13.88c6.17-6.31 17.86-15.54 34-16.32z" fill="#bdc3c7"/><path id="Shape" d="m24 55h-6c-.5522847 0-1-.4477153-1-1s.4477153-1 1-1h6c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z" fill="#2c3e50"/><g fill="#547580"><path id="Shape" d="m26 10h-10c-.5522847 0-1-.44771525-1-1s.4477153-1 1-1h10c.5522847 0 1 .44771525 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m34 15h-26c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h26c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m34 20h-26c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h26c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m34 25h-26c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h26c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m26.81 30h-18.81c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h18.81c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m26.7 36h-18.7c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h18.7c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m21 41h-13c-.55228475 0-1-.4477153-1-1s.44771525-1 1-1h13c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/><path id="Shape" d="m22 46h-2c-.5522847 0-1-.4477153-1-1s.4477153-1 1-1h2c.5522847 0 1 .4477153 1 1s-.4477153 1-1 1z"/></g><path id="Shape" d="m54 36.89v5.95c-.0017571 2.0935992-.3393804 4.173359-1 6.16s-.9982429 4.0664008-1 6.16v1.84c0 .5522847-.4477153 1-1 1h-14c-.5522847 0-1-.4477153-1-1v-5.51c.0015758-.3100364-.1429139-.6027207-.39-.79l-7.27-5.66c-.8579789-.6630797-1.4043108-1.6508187-1.51-2.73l-.52-5.16c-.1843214-1.7882394.8645532-3.4746652 2.55-4.1l-3.6-7.12c-.3223054-.6880136-.346175-1.4785879-.0659649-2.184793.2802101-.7062052.8396224-1.2653432 1.5459649-1.545207.324571-.1296299.6705092-.197461 1.02-.2 1.0145789.0012746 1.9470946.557714 2.43 1.45l3.78 6.57.15-.09 1.45-.87c.6462804-.3943502 1.3932374-.5923807 2.15-.57.5315858.0124109 1.0552686.1314297 1.54.35.79-.35 2.17-1.79 4.37-1.22.5288124.1478819 1.024407.3956792 1.46.73.3289268-.2178862.6770344-.4053288 1.04-.56 1.822862-.7452092 3.9167796-.0486524 4.93 1.64l.94 1.57 1.16 2.31c.551398 1.1129235.8388392 2.3379706.84 3.58z" fill="#fdd7ad"/><path id="Shape" d="m37 35.28-3.03-5.26.15-.09 1.45-.87.242.063 1.628 2.82c.6252385 1.0837666.4447857 2.4523376-.44 3.337z" fill="#f9c795"/><path id="Shape" d="m31.28 37.26-.15.09c-.2835598.1712324-.6087519.2611791-.94.26-.6596393.0021249-1.2700029-.3488341-1.6-.92l-1.41-2.44c.4453456-.5401175 1.0246417-.9539004 1.68-1.2z" fill="#f9c795"/><path id="Shape" d="m42.223 31.945-.223.055-.94-1.57c-.4201149-.7042385-1.0492951-1.2600143-1.8-1.59l1.24-.74c1.0695391.2363493 1.9950753.9015785 2.56 1.84.2202852.367735.2528326.8182335.0876949 1.2138143-.1651377.3955807-.5083317.6892237-.9246949.7911857z" fill="#f9c795"/><path id="Shape" d="m47 31-.94-1.57c-.2497148-.4198815-.5792511-.786788-.97-1.08l.48-.29c.177461-.1079623.3650125-.198389.56-.27.3443903.1413067.6674787.329775.96.56l.011.008c.6260122.5088077.74981 1.4160753.283 2.074z" fill="#f9c795"/></g></g></svg><span><?php echo $guides_block['guides_bottom_title']; ?></span></h2>
            </div>
            <div class="casino-guides-container">
                <?php $query = new WP_Query(array('posts_per_page' => -1, 'cat' => $category_id)); ?>
                <?php $posts = $query->posts; ?>
                <?php foreach ($posts as $key => $post) : ?>
                <div class="casino-guides-box">
                    <a href="<?php echo get_permalink($post); ?>">
                        <div class="casino-guides-box-logo">
                            <?php echo get_field('icon', $post->ID); ?>
                        </div>
                        <div class="casino-guides-box-text">
                            <p><?php echo $post->post_title; ?>. <span><?php echo get_field('text', $post->ID); ?></span></p>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <?php $description = get_field('description'); ?>
            <?php foreach ($description as $text) : ?>
                <div class="casino-guides-paragraph">
                    <h2><?php echo $text['title']; ?></h2>
                    <p><?php echo nl2br($text['text']); ?></p>
                </div>
            <?php endforeach; ?>
            <?php if (!empty(get_field('slider_bot'))) : ?>
            <div class="slide">
                <?php $slider_bot = get_field('slider_bot'); ?>
                <?php foreach ($slider_bot as $bot) : ?>
                <div class="slide-casino">
                    <div class="slider-container-casino">
                        <div class="slide-casino-content">
                            <div class="slide-casino-logo">
                                <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $bot->ID); ?>"></a>
                            </div>
                            <div class="slide-casino-bonus">
                                <h3><?php echo get_field('bonus', $bot->ID); ?></h3>
                                <h4><?php echo get_field('bonus_bottom_text', $bot->ID); ?></h4>
                            </div>
                            <div class="slide-casino-name">
                                <p><?php echo $bot->post_title; ?></p>
                            </div>
                        </div>
                        <div class="slide-casino-button">
                            <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank">Visit<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" class="svg-inline--fa fa-angle-double-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg></a>
                        </div>
                    </div>
                    <?php if (!empty(get_field('terms_info', $bot->ID))) : ?>
                    <div class="slide-terms">
                        <p><?php echo get_field('terms_info', $bot->ID); ?>
                            <a href="<?php echo get_field('terms_link', $bot->ID); ?>" target="_blank">18+. Terms & Conditions apply</a><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                        </p>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
