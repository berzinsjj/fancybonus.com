<?php /* Template Name: Casinos */ ?>
<?php get_header(); ?>
<div class="page-container">
    <div class="page-content page-casinos">
        <div class="recommended-casinos">
            <div class="recommended-casinos-text">
                <?php $featured_top_companies_block = get_field('featured_top_companies_block'); ?>
                <h2><?php echo $featured_top_companies_block['title']; ?><svg version="1.1" id="Capa_1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 244.4 216.7" style="enable-background:new 0 0 244.4 216.7;" xml:space="preserve"><metadata><sfw  xmlns="&ns_sfw;"><slices></slices><sliceSourceBounds  bottomLeftOrigin="true" height="217.1" width="244.3" x="0" y="-0.1"></sliceSourceBounds></sfw></metadata><path class="st0" d="M226.6,113.8h-5.1c9.9,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16-17.7,16h-8.3c9.9,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16-17.7,16h-4c9.8,0,17.8,8.3,17.1,18.3c-0.6,9.1-8.6,16.1-17.7,16.1H95.8c-10.7,0-19.3-8.7-19.3-19.3v-79.2l43.3-69.6c1.5-2.4,2.1-5.1,1.8-7.9l-2.7-27C118.3,7,122.8,0.9,129.4,0c10.6-1.5,25.8,2.1,33.7,30.6c0,0,2.3,33.6-3.7,48.8h67.8c9.9,0,17.8,8.3,17.1,18.3C243.7,106.9,235.7,113.8,226.6,113.8z"/><path class="st1" d="M62.6,101.6H13.8c-7.6,0-13.8,6.2-13.8,13.8v87.5c0,7.6,6.2,13.8,13.8,13.8h48.8c7.6,0,13.8-6.2,13.8-13.8v-87.5C76.5,107.8,70.3,101.6,62.6,101.6z"/><circle class="st2" cx="38.2" cy="202.7" r="8.5"/></svg><span><?php echo $featured_top_companies_block['bottom_title']; ?></span></h2>
            </div>
            <?php if (!empty($featured_top_companies_block['featured_top_companies'])) : ?>
                <div class="recommended-casinos-container">
                    <?php foreach ($featured_top_companies_block['featured_top_companies'] as $key => $featured_company) : ?>
                        <div class="recommended-casinos-box <?php echo $key === 0 ? 'background-casino' : ''; ?>">
                            <div class="recommended-casinos-box-name">
                                <p><?php echo $featured_company->post_title; ?></p>
                            </div>
                            <div class="recommended-casinos-box-rating">
                                <div class="star-ratings">
                                    <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_company->ID); ?>%">
                                        <span>●●●●●</span>
                                    </div>
                                    <div class="empty-ratings">
                                        <span>●●●●●</span>
                                    </div>
                                </div>
                            </div>
                            <div class="recommended-casinos-box-logo">
                                <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank"> <img src="<?php echo get_field('big_logo', $featured_company->ID); ?>"></a>
                            </div>
                            <div class="recommended-casinos-box-bonus">
                                <h3><?php echo get_field('bonus', $featured_company->ID); ?></h3>
                                <h4><?php echo get_field('bonus_bottom_text', $featured_company->ID); ?></h4>
                            </div>
                            <div class="recommended-casinos-box-terms-link">
                                <a href="<?php echo get_field('terms_link', $featured_company->ID); ?>" target="_blank">18+. Terms & Conditions apply</a>
                                <div class="recommended-casinos-box-svg">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                                </div>
                            </div>
                            <div class="mawkurs">
                                <div class="recommended-casinos-box-button-info">
                                    <div class="recommended-casinos-box-button">
                                        <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank">Visit Casino</a>
                                    </div>
                                    <?php if (!empty(get_field('terms_info', $featured_company->ID))) : ?>
                                        <div class="recommended-casinos-box-terms-info">
                                            <p><?php echo get_field('terms_info', $featured_company->ID); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <div class="online-casinos">
                <div class="online-casinos-text">
                    <?php $featured_bottom_block = get_field('featured_bottom_casino_block'); ?>
                    <h2><?php echo $featured_bottom_block['title']; ?><svg height="512" viewBox="0 0 192 192" width="512" xmlns="http://www.w3.org/2000/svg"><path fill="#ffc83d" d="m155.109 74.028a4 4 0 0 0 -3.48-2.028h-52.4l8.785-67.123a4.023 4.023 0 0 0 -7.373-2.614l-63.724 111.642a4 4 0 0 0 3.407 6.095h51.617l-6.962 67.224a4.024 4.024 0 0 0 7.411 2.461l62.671-111.63a4 4 0 0 0 .048-4.027z"/></svg><span><?php echo $featured_bottom_block['bottom_title']; ?></span></h2>
                </div>
            </div>
            <?php if (!empty($featured_bottom_block['featured_bot_companies'])) : ?>
                <div class="best-online-casinos-container">
                    <?php foreach ($featured_bottom_block['featured_bot_companies'] as $key => $featured_company) : ?>
                    <div class="best-online-casinos-box">
                        <div class="best-nr">
                            <p><?php echo $key + 1; ?></p>
                        </div>
                        <div class="best-logo">
                            <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $featured_company->ID); ?>"></a>
                        </div>
                        <div class="best-name-rating">
                            <div class="best-name">
                                <p><?php echo $featured_company->post_title; ?></p>
                            </div>
                            <div class="best-rating">
                                <div class="star-ratings">
                                    <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_company->ID); ?>%">
                                        <span>●●●●●</span>
                                    </div>
                                    <div class="empty-ratings">
                                        <span>●●●●●</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="best-bonus">
                            <h3><?php echo get_field('bonus', $featured_company->ID); ?></h3>
                            <h4><?php echo get_field('bonus_bottom_text', $featured_company->ID); ?></h4>
                        </div>
                        <div class="best-button-link">
                            <div class="best-button">
                                <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank">Visit Casino</a>
                            </div>
                            <div class="best-link">
                                <a href="<?php echo get_field('terms_link', $featured_company->ID); ?>" target="_blank">18+. Terms & Conditions apply</a>
                                <div class="best-link-svg"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></div>
                            </div>
                        </div>
                    </div>
                    <div class="best-online-casinos-paragraph">
                        <?php if (!empty(get_field('terms_info', $featured_company->ID))) : ?>
                        <p><?php echo get_field('terms_info', $featured_company->ID); ?></p>
                        <?php endif; ?>
                    </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="casino-guides">
            <?php $description = get_field('description'); ?>
            <?php foreach ($description as $text) : ?>
            <div class="casino-guides-paragraph">
                <h2><?php echo $text['title']; ?></h2>
                <p><?php echo nl2br($text['text']); ?></p>
            </div>
            <?php endforeach; ?>
            <?php if (!empty(get_field('slider_bot'))) : ?>
            <div class="slide">
                <?php $slider_bot = get_field('slider_bot'); ?>
                <?php foreach ($slider_bot as $bot) : ?>
                    <div class="slide-casino">
                        <div class="slider-container-casino">
                            <div class="slide-casino-content">
                                <div class="slide-casino-logo">
                                    <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $bot->ID); ?>"></a>
                                </div>
                                <div class="slide-casino-bonus">
                                    <h3><?php echo get_field('bonus', $bot->ID); ?></h3>
                                    <h4><?php echo get_field('bonus_bottom_text', $bot->ID); ?></h4>
                                </div>
                                <div class="slide-casino-name">
                                    <p><?php echo $bot->post_title; ?></p>
                                </div>
                            </div>
                            <div class="slide-casino-button">
                                <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank">Visit<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" class="svg-inline--fa fa-angle-double-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg></a>
                            </div>
                        </div>
                        <?php if (!empty(get_field('terms_info', $bot->ID))) : ?>
                        <div class="slide-terms">
                            <p><?php echo get_field('terms_info', $bot->ID); ?>
                                <a href="<?php echo get_field('terms_link', $bot->ID); ?>" target="_blank">18+. Terms & Conditions apply</a><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                            </p>
                        </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
