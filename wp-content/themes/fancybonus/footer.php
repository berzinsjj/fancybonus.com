<footer>
    <?php $footer = get_field('footer', 8); ?>
    <div class="footer-menu-background">
        <div class="footer-logo-text-menu">
            <div class="footer-logo-text">
                <a href="/"><img src="<?php echo $footer['footer_logo']; ?>"></a>
                <p><?php echo $footer['footer_text']; ?></p>
            </div>
            <div class="footer-menus">

                <div class="footer-menu">
                    <p>Fancy Bonus</p>
                    <?php echo wp_nav_menu(array('menu' => 'Footer menu')); ?>
                </div>

                <div class="footer-menu-2">
                    <p>Legal</p>
                    <?php echo wp_nav_menu(array('menu' => 'Footer menu 2')); ?>
                </div>
            </div>
        </div>
        <div class="footer-logo">
            <?php foreach ($footer['footer_icons'] as $icon) : ?>
                <a href="<?php echo $icon['link']; ?>" target="_blank">
                    <img src="<?php echo $icon['icon']; ?>" class="footer-icon">
                    <img src="<?php echo $icon['icon_hover']; ?>" class="footer-icon-hover">
                </a>
            <?php endforeach; ?>
        </div>
        <div class="copyright">
            <p><?php echo $footer['copyright']; ?></p>
        </div>
    </div>
    <script type="text/javascript" src="/wp-content/themes/fancybonus/assets/js/slick.js"></script>
</footer>
<?php wp_footer(); ?>
</body>
</html>