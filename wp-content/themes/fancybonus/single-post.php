<?php get_header(); ?>
<?php $page = get_post(get_the_ID()); ?>
    <div class="page-container">
        <div class="page-content single-post">
            <div class="casino-guides">
                <div class="casino-page-info-paragraph info-info">
                    <h1><?php echo nl2br($page->post_title); ?></h1>
                       <?php if (!empty(get_field('text'))) : ?>
                           <p><?php echo get_field('text'); ?></p>
                       <?php endif; ?>
                </div>
            </div>
            <div class="recommended-casinos">
                <?php $featured_top_companies_block = get_field('featured_top_companies_block'); ?>
                <?php if (!empty($featured_top_companies_block['featured_top_companies'])) : ?>
                    <div class="recommended-casinos-container">
                        <?php foreach ($featured_top_companies_block['featured_top_companies'] as $key => $featured_company) : ?>
                            <div class="recommended-casinos-box <?php echo $key === 0 ? 'background-featured' : ''; ?>">
                                <div class="recommended-casinos-box-name">
                                    <p><?php echo $featured_company->post_title; ?></p>
                                </div>
                                <div class="recommended-casinos-box-rating">
                                    <div class="star-ratings">
                                        <div class="fill-ratings" style="width: <?php echo get_field('rating', $featured_company->ID); ?>%">
                                            <span>●●●●●</span>
                                        </div>
                                        <div class="empty-ratings">
                                            <span>●●●●●</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="recommended-casinos-box-logo">
                                    <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank"> <img src="<?php echo get_field('big_logo', $featured_company->ID); ?>"></a>
                                </div>
                                <div class="recommended-casinos-box-bonus">
                                    <h3><?php echo get_field('bonus', $featured_company->ID); ?></h3>
                                    <h4><?php echo get_field('bonus_bottom_text', $featured_company->ID); ?></h4>
                                </div>
                                <div class="recommended-casinos-box-terms-link">
                                    <a href="<?php echo get_field('terms_link', $featured_company->ID); ?>" target="_blank">18+. Terms & Conditions apply</a>
                                    <div class="recommended-casinos-box-svg">
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                                    </div>
                                </div>
                                <div class="mawkurs">
                                    <div class="recommended-casinos-box-button-info">
                                        <div class="recommended-casinos-box-button">
                                            <a href="<?php echo get_field('affialite_link', $featured_company->ID); ?>" target="_blank">Visit Casino</a>
                                        </div>
                                        <?php if (!empty(get_field('terms_info', $featured_company->ID))) : ?>
                                            <div class="recommended-casinos-box-terms-info">
                                                <p><?php echo get_field('terms_info', $featured_company->ID); ?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="casino-guides">
                <div class="casino-page-info-paragraph info-info-second">
                    <?php echo nl2br($page->post_content); ?>
                    <div class="casino-page-button">
                        <input class="back-button" type="button" value="Go Back" onclick="window.history.back()" />
                    </div>
                </div>
            </div>
            <?php if (!empty(get_field('slider_bot'))) : ?>
                <div class="slide">
                <?php $slider_bot = get_field('slider_bot'); ?>
                <?php foreach ($slider_bot as $bot) : ?>
                    <div class="slide-casino">
                        <div class="slider-container-casino">
                            <div class="slide-casino-content">
                                <div class="slide-casino-logo">
                                    <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank"> <img src="<?php echo get_field('logo', $bot->ID); ?>"></a>
                                </div>
                                <div class="slide-casino-bonus">
                                    <h3><?php echo get_field('bonus', $bot->ID); ?></h3>
                                    <h4><?php echo get_field('bonus_bottom_text', $bot->ID); ?></h4>
                                </div>
                                <div class="slide-casino-name">
                                    <p><?php echo $bot->post_title; ?></p>
                                </div>
                            </div>
                            <div class="slide-casino-button">
                                <a href="<?php echo get_field('affialite_link', $bot->ID); ?>" target="_blank">Visit<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-double-right" class="svg-inline--fa fa-angle-double-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34zm192-34l-136-136c-9.4-9.4-24.6-9.4-33.9 0l-22.6 22.6c-9.4 9.4-9.4 24.6 0 33.9l96.4 96.4-96.4 96.4c-9.4 9.4-9.4 24.6 0 33.9l22.6 22.6c9.4 9.4 24.6 9.4 33.9 0l136-136c9.4-9.2 9.4-24.4 0-33.8z"></path></svg></a>
                            </div>
                        </div>
                        <?php if (!empty(get_field('terms_info', $bot->ID))) : ?>
                        <div class="slide-terms">
                            <p><?php echo get_field('terms_info', $bot->ID); ?>
                                <a href="<?php echo get_field('terms_link', $bot->ID); ?>" target="_blank">18+. Terms & Conditions apply</a><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
                            </p>
                        </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>