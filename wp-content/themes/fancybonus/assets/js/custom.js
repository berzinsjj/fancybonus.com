jQuery(function ($) {
    jQuery(document).ready(function () {
        // Mobile menu
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-main-menu-container").toggleClass("is-active");
        });

        // Gets the span width of the filled-ratings span
        // this will be the same for each rating
        var star_rating_width = $('.fill-ratings span').width();
        // Sets the container of the ratings to span width
        // thus the percentages in mobile will never be wrong
        $('.star-ratings').width(star_rating_width);

        /*sliders*/
        $('.slide').slick({
            dots: false,
            arrows: true,
            infinite: true,
            prevArrow:"<img class='a-left control-c prev slick-prev' src='/wp-content/themes/fancybonus/assets/images/kreisi.png'>",
            nextArrow:"<img class='a-right control-c next slick-next' src='/wp-content/themes/fancybonus/assets/images/labi.png'>",
            autoplay: 5000
        });

        // Paragraph display/hide
        $(".online-casinos-box-terms-link").hover(function() {
            $(this).parent().parent().find(".hide-paragraph").toggle();
            $(this).parent().parent().find(".online-casinos-inside").toggleClass("opened");
        });
    });
});