<?php /* Template Name: Info */ ?>
<?php get_header(); ?>
<?php $page = get_post(get_the_ID()); ?>
    <div class="page-container">
        <div class="page-content page-info">
            <div class="casino-guides">
                <div class="casino-page-info-paragraph">
                    <?php echo nl2br($page->post_content); ?>
                </div>
            </div>
        </div>
</div>
<?php get_footer(); ?>
